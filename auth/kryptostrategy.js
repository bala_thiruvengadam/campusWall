var util = require('util'),
    passport = require('passport-strategy'),
    dbUtil = require('../config/dbUtil'),
    request = require('request-promise'),
    loginURL = "https://kryptosda.kryptosmobile.com/kryptosds/nuser/auth";

function CustomStrategy(options) {
    passport.Strategy.call(this);
}
util.inherits(CustomStrategy, passport.Strategy);

CustomStrategy.prototype.authenticate = function(req, options) {
    //console.log('Options: ', options);
    var self = this;
    this.name = "student";
    var requestData = req.body;
    // console.log(" requestData ", requestData);
    const reqOptions = {
        method: 'POST',
        uri: loginURL,
        body: requestData,
        json: true
    };
    request(reqOptions).then((response) => {
        if (response.error) {
            console.log("error : ", response.error);

            self.redirect(options.failureRedirect);
        }
        if (response.success) {
            var user = {
                username: req.body.username,
                tenant: req.body.tenant,
                role: response.userinfo.studentRole,
                platform: 'kryptosmobile',
                firstname: response.userinfo.firstname,
                studentRole: response.userinfo.studentRole,
                userid: response.userinfo._id
            };

           self.success(user);

        } else {
           return self.fail();
        }
    }).catch(function(err) {
        console.log("err : ", err.error);
       return self.fail();
    });

}
module.exports = CustomStrategy;