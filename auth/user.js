var mongoose = require('../config/mongoose').Mongoose;

var Schema = mongoose.Schema;

var User = new Schema({
	userId: {
		type: String,
		unique: true
	},
	name: String,
	email: {
		type: String,
		required: [true, "Email required "]
	},
	password: {
		type: String,
		required: [true, "Password required"]
	},
	mobile: {
		cCode: String,
		no: String
	},
	status: {
		type: String,
		enum: ['ACTIVE', 'INACTIVE', 'DORMANT', 'DISABLED', 'BLOCKED', 'UNVERFIED'],
		default: 'UNVERFIED'
	},
	role: {
		type: [Number],
		default: [1]
	},
	created_on: {
		type: Date,
		default: Date.now
	}

});

module.exports = mongoose.model('User', User);