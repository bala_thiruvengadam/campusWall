var MongoClient = require('mongodb').MongoClient,
    config = require("./config"),
    db;
module.exports.getConnection = function(callback) {
    if (!db) {
        MongoClient.connect(config.mongoUrl, function(err, database) {
            if (err) {
                throw err;
            }
            console.log("Created database connection in db connection" + database);
            db = database;
            callback(db);
        });
    } else {
        console.log("Else block of get connection");
        callback(db);
    }
}
module.exports.getConn = function() {
    var connPromise = new Promise(function(resolve, reject) {
        if (!db) {
            MongoClient.connect(config.mongoUrl, function(err, database) {
                if (err) {
                    reject(err);
                }
                console.log("Created database connection in db connection (using promise)" + database);
                db = database;
                resolve(db);
            });
        } else {
            resolve(db);
        }
    });
    return connPromise;
};